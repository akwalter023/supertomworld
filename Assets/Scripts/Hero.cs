﻿using UnityEngine;
using System.Collections;

// NOTES:
// Moving platforms must be tagged as MovingFloor
// OnCharacterHitFromBelow sent to objects when hero's head hits them
// OnCharacterLandedOn sent to objects the player lands on
// OnCharacterLeft sent to objects when the player is no longer on them

public class Hero : MonoBehaviour {

	// cached objects for performance
	private Transform t;
	private Rigidbody r;
	private BoxCollider feetCollider;

	// movement constants
	private float speed = 3f;
	private float jumpForce = 500;

	// store the maximum value of x the player has moved
	private float maxX = 0;
	private static float MAX_X_DEVIATION = 8.5f;

	// true if on ground
	private bool grounded = false;

	// cache static camera position data
	private float cameraY = 1f;
	private float cameraZ;

	void Start() {
		// cache components
		this.t = GetComponent<Transform> ();
		this.r = GetComponent<Rigidbody> ();
		this.feetCollider = GetComponentInChildren<Feet>().GetComponent<BoxCollider> ();

		// camera z should not change - cache it now
		Vector3 cameraPos = Camera.main.transform.position;
		cameraZ = cameraPos.z;
	}

	void Update() {
		// get desired new x position
		float newX = t.position.x + (Input.GetAxis ("Horizontal") * Time.deltaTime * speed);

		// see if it is larger than the max x value
		if (newX > maxX) {
			maxX = newX;
		}

		// calculate the minimum x value - user can't go backwards
		float minX = maxX - MAX_X_DEVIATION;

		// clamp desired position 
		newX = Mathf.Clamp (newX, minX, 118);

		// determine new position
		Vector3 pos = new Vector3(newX, t.position.y, t.position.z);
		t.position = pos;

		// jump
		if (Input.GetButtonDown("Jump") && grounded) {
			// If we are on a moving platform, we need to disable collision detection
			// so that the player isn't automatically reparented to the platform
			// and then manually execute the code that happens when exiting the platform
			// specifically, unparent and set kinematic to false. Without doing this,
			// the user is unable to jump from a moving platform. After this is complete,
			// wait a brief period of time for the user to exit the collider of the 
			// moving platform and then renable the box collider.
			if (t.parent != null) {
				feetCollider.enabled = false;
				t.parent = null;
				r.isKinematic = false;
				StartCoroutine ("EnableBoxCollider");
			}

			// add force to jump
			r.AddForce (new Vector3 (0, jumpForce, 0));
			grounded = false;

		}
	}

	// reenable the box collider after a brief period
	private IEnumerator EnableBoxCollider() {
		yield return new WaitForSeconds (.1f);
		feetCollider.enabled = true;
	}
		
	void LateUpdate() {
		float playerX = t.position.x;
		float cameraX = Camera.main.transform.position.x;

		// don't let the player go back
		if (playerX > cameraX) {
			float newCameraX = Mathf.Clamp (playerX, -1, 100);
			Vector3 pos = new Vector3 (newCameraX, cameraY, cameraZ);
			Camera.main.transform.position = pos;
		}
	}

	// The hero object has a child collider at the feet to detect when grounded
	public void OnFeetHitGround(Collider other) {
		grounded = true;

		// if moving floor, parent this object to moving platform
		if (other.gameObject.tag == "MovingFloor") {
			t.parent = other.transform;
			r.isKinematic = true;
		}

		// trigger any events that should happen when the player lands on this object
		other.gameObject.SendMessage ("OnCharacterLandedOn", SendMessageOptions.DontRequireReceiver);
	}

	public void OnFeetLeftGround(Collider other) {
		// if leaving moving floor, unparent
		if (other.gameObject.tag == "MovingFloor") {
			t.parent = null;
			r.isKinematic = false;
		}

		// trigger any events that should happen when a player leaves the ground
		other.gameObject.SendMessage("OnCharacterLeft", SendMessageOptions.DontRequireReceiver);
	}

	public void OnHeadHitObject(Collider other) {
		other.gameObject.SendMessage ("OnCharacterHitFromBelow", SendMessageOptions.DontRequireReceiver);
	}
}
