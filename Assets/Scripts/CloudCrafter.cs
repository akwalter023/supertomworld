﻿using UnityEngine;
using System.Collections;

public class CloudCrafter : MonoBehaviour {

	public int numClouds;
	public GameObject[] cloudPrefabs;
	public Vector3 cloudMinPosition;
	public Vector3 cloudMaxPosition;
	public float cloudMinScale = 1;
	public float cloudMaxScale = 5;
	public float cloudSpeedMultiplier = .5f;

	private GameObject[] clouds;

	void Awake() {
		clouds = new GameObject[numClouds];

		GameObject cloud;
		for (int i = 0; i < numClouds; ++i) {
			// create cloud
			int prefabNum = Random.Range (0, cloudPrefabs.Length);
			cloud = Instantiate (cloudPrefabs [prefabNum]) as GameObject;

			// determine position
			Vector3 pos = Vector3.zero;
			pos.x = Random.Range (cloudMinPosition.x, cloudMaxPosition.x);
			pos.y = Random.Range (cloudMinPosition.y, cloudMaxPosition.y);

			//determine scale
			float scaleU = Random.value;
			float scaleVal = Mathf.Lerp (cloudMinScale, cloudMaxScale, scaleU);

			// smaller clouds should be nearer to the ground
			pos.y = Mathf.Lerp(cloudMinPosition.y, pos.y, scaleU);

			// smaller clouds further away
			pos.z = 100 - 90 * scaleU;

			// apply to object
			cloud.transform.position = pos;
			cloud.transform.localScale = Vector3.one * scaleVal;

			// set parent
			cloud.transform.parent = this.transform;

			// add to array
			clouds[i] = cloud;
		}
	}

	void Update() {
		// clouds move left
		foreach (GameObject cloud in clouds) {
			// get position & scale
			float scale = cloud.transform.localScale.x;
			Vector3 pos = cloud.transform.position;

			// move larger clouds (nearer) faster
			pos.x -= scale * Time.deltaTime * cloudSpeedMultiplier;

			// if cloud moved off the left, move to far right
			if (pos.x <= cloudMinPosition.x) {
				pos.x = cloudMaxPosition.x;
			}

			// now move cloud
			cloud.transform.position = pos;
		}
	}
}
