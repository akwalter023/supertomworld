﻿using UnityEngine;
using System.Collections;

public class Pit : MonoBehaviour {

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			GameController.Instance.RestartGame();
		}
	}
}
