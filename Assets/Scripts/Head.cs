﻿using UnityEngine;
using System.Collections;

public class Head : MonoBehaviour {

	private Hero player;

	void Start() {
		player = this.transform.parent.gameObject.GetComponent<Hero> ();
	}

	void OnTriggerEnter(Collider other) {
		player.OnHeadHitObject (other);
	}
}
